<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ReportTransactionRequest;
use App\Models\PaymentMethod;
use App\Models\ReportTransaction;
use App\Models\StoreBranch;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReportTransactionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReportTransactionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ReportTransaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/report-transaction');
        CRUD::setEntityNameStrings('report transaction', 'report transactions');
        $this->crud->setShowView('reporttransaction.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if (backpack_user()->hasAnyRole(['store manager', 'regional manager'])) {
            $this->crud->addClause('whereIn', 'store_branch_id', backpack_user()->storeBranch->pluck('id'));
        }
        $this->crud->addFilter([
            'name'  => 'transaction_number',
            'type'  => 'select2_multiple',
            'label' => 'Transaction Number',
          ], function() {
              return ReportTransaction::pluck('transaction_number', 'id')->toArray();
          }, function($values) { // if the filter is active
              $this->crud->addClause('whereIn', 'id', json_decode($values));
          });
        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'Date Range'
          ],
          false,
          function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'created_at', '>=', $dates->from);
            $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
          });
        $this->crud->addFilter([
            'name'  => 'payment_method',
            'type'  => 'select2_multiple',
            'label' => 'Filter Payment Method',
          ], function() {
              return PaymentMethod::whereIn('id', ReportTransaction::pluck('payment_method')->toArray())->pluck('name', 'id')->toArray();
          }, function($values) { // if the filter is active
              $this->crud->addClause('whereIn', 'payment_method', json_decode($values));
          });
        $this->crud->addFilter([
            'name'  => 'user',
            'type'  => 'select2_multiple',
            'label' => 'Filter Pengguna',
          ], function() {
              return User::whereIn('id', ReportTransaction::pluck('user_id')->toArray())->pluck('name', 'id')->toArray();
          }, function($values) { // if the filter is active
              $this->crud->addClause('whereIn', 'user_id', json_decode($values));
          });
        $this->crud->addFilter([
            'name'  => 'storebranch',
            'type'  => 'select2_multiple',
            'label' => 'Filter Store Branch',
          ], function() {
              return StoreBranch::whereIn('id', ReportTransaction::pluck('store_branch_id')->toArray())->pluck('name', 'id')->toArray();
          }, function($values) { // if the filter is active
              $this->crud->addClause('whereIn', 'store_branch_id', json_decode($values));
          });

        $this->crud->removeAllButtons();
        if (backpack_user()->hasAnyRole(['regional manager', 'admin', 'superadmin'])) {
            $this->crud->addButtonFromView('line', 'update', 'refund', 'beginning');
        }
        $this->crud->addButtonFromView('line', 'preview', 'show', 'beginning');

        $this->crud->enableExportButtons();
        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Number',
            'orderable' => false,
        ])->makeFirstColumn();
        $this->crud->addColumn([
            'name'      => 'transaction_number',
            'type'      => 'type',
            'label'     => 'Transaction Number',
        ]);
        $this->crud->addColumn([
            'name'     => 'total_amount',
            'label'    => 'Total Amount',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->total_amount, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Payment Method', // Table column heading
            'type'      => 'select',
            'name'      => 'payment_method', // the column that contains the ID of that connected entity;
            'entity'    => 'paymentMethod', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\PaymentMethod", // foreign key model
        ]);
        $this->crud->addColumn([
            'name'     => 'cash',
            'label'    => 'Cash',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->cash, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'change',
            'label'    => 'Change',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->change, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            'name'     => 'refund',
            'label'    => 'Refund',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->refund, 2, ',', '.');
            }
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Kasir', // Table column heading
            'type'      => 'select',
            'name'      => 'user_id', // the column that contains the ID of that connected entity;
            'entity'    => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\User", // foreign key model
        ]);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Store', // Table column heading
            'type'      => 'select',
            'name'      => 'store_branch_id', // the column that contains the ID of that connected entity;
            'entity'    => 'storebranch', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\StoreBranch", // foreign key model
        ]);
        $this->crud->addColumn([
            'name'     => 'detail',
            'label'    => 'Item',
            'type'     => 'closure',
            'function' => function($entry) {
                return count($entry->details);
            },
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('transaction-detail?payment_method=%5B"'.$entry->id.'"%5D');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReportTransactionRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
