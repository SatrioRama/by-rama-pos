<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Color;
use App\Http\Controllers\Admin\AdminController;
use App\Investor;
use App\Models\TransactionDetail;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class InvestorMonthlyChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InvestorMonthlyChartController extends ChartController
{
    public function setup()
    {
        $db = new AdminController;

        $db->investor();

        $this->chart = new Chart();
        if (count(backpack_user()->storeBranch) > 0) {
            foreach (backpack_user()->storeBranch as $key => $store) {
                $data = [];
                $modal_perhari = $store->operational/count($db->data['investor']['monthly']);
                foreach ($db->data['investor']['monthly'] as $key => $date) {
                    $check = TransactionDetail::whereIn('product_id', backpack_user()->userProduct->pluck('id'))
                    ->whereHas('transaction', function (Builder $query) use ($store) {
                        $query->where('store_branch_id', '=', $store->id);
                    })
                    ->where('created_at', '>=', backpack_user()->date_join)
                    ->whereDate('created_at', date('Y-m-d', strtotime($date)));
                    $data[] = Investor::calc($check->sum('sub_total'), $modal_perhari, $store->tax, backpack_user()->percentage);
                }
                $warna = Color::hex2rgb($store->background_color);
                $this->chart->dataset($store->name, 'bar', $data)->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
            }
        } else {
            $color = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $warna = Color::hex2rgb('#'.$color);
            $this->chart->dataset('No Data', 'bar', [0])->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
        }

        // OPTIONAL
        $this->chart->minimalist(true);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($db->data['investor']['monthly']);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
