<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Color;
use App\Http\Controllers\Admin\AdminController;
use App\Models\Product;
use App\Models\TransactionDetail;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class RegionalManagerPopularProductChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RegionalManagerPopularProductChartController extends ChartController
{
    public function setup()
    {
        $db = new AdminController;

        $db->regionalManager();

        $this->chart = new Chart();

        $label = [];
        foreach ($db->data['regional_manager']['popular']['store_id'] as $key => $value) {
            $products = $value->product;
            if (count($products) > 0) {
                foreach ($products as $product) {
                    $data = [];
                    foreach ($db->data['regional_manager']['monthly'] as $key => $date) {
                        $sum = TransactionDetail::whereDate('created_at', date('Y-m-d', strtotime($date)))->where('product_id', '=', $product->id)->sum('sub_total');
                        $data[] = (int)$sum;
                    }
                    $color = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
                    $warna = Color::hex2rgb('#'.$color);
                    $this->chart->dataset($product->name." - ".$product->storeBranch->first()->name, 'bar', $data)->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
                }
            } else {
                $color = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
                $warna = Color::hex2rgb('#'.$color);
                $this->chart->dataset('No Data', 'bar', [0])->color('rgb('.$warna[0].', '.$warna[1].', '.$warna[2].')')->backgroundColor('rgba('.$warna[0].', '.$warna[1].', '.$warna[2].', 0.4)');
            }
        }

        // OPTIONAL
        $this->chart->minimalist(true);
        $this->chart->displayAxes(true);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($db->data['regional_manager']['monthly']);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
    //     $users_created_today = \App\User::whereDate('created_at', today())->count();

    //     $this->chart->dataset('Users Created', 'bar', [
    //                 $users_created_today,
    //             ])
    //         ->color('rgba(205, 32, 31, 1)')
    //         ->backgroundColor('rgba(205, 32, 31, 0.4)');
    // }
}
