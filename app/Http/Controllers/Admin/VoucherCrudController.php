<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VoucherRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class VoucherCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VoucherCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Voucher::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/voucher');
        CRUD::setEntityNameStrings('voucher', 'vouchers');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VoucherRequest::class);

        $this->crud->addFields(['voucher_code', 'name', 'disc']);
        $this->crud->addField([   // Number
            'name' => 'disc',
            'label' => 'Discount',
            'type' => 'number',

            // optionals
            // 'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            'suffix'     => ".00",
        ]);

        $this->crud->addField([   // date_range
            'name'  => ['start_date', 'end_date'], // db columns for start_date & end_date
            'label' => 'Voucher Range',
            'type'  => 'date_range',

            // OPTIONALS
            // default values for start_date & end_date
            // 'default'            => ['2019-03-28 01:01', '2019-04-05 02:00'],
            // options sent to daterangepicker.js
            'date_range_options' => [
                'drops' => 'auto', // can be one of [down/up/auto]
                'timePicker' => false,
                // 'locale' => ['format' => 'DD/MM/YYYY HH:mm']
            ]
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Product",
            'type'      => 'select2_multiple',
            'name'      => 'product', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'product', // the method that defines the relationship in your Model
            'model'     => "App\Models\Product", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            // 'options'   => (function ($query) {
            //     return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            // }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Store",
            'type'      => 'select2_multiple',
            'name'      => 'store', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'store', // the method that defines the relationship in your Model
            'model'     => "App\Models\StoreBranch", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            // 'options'   => (function ($query) {
            //     return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            // }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->addFields(['voucher_code', 'name', 'disc']);
        $this->crud->addField([   // Number
            'name' => 'disc',
            'label' => 'Discount',
            'type' => 'number',

            // optionals
            // 'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            'suffix'     => ".00",
        ]);

        $this->crud->addField([   // date_range
            'name'  => ['start_date', 'end_date'], // db columns for start_date & end_date
            'label' => 'Voucher Range',
            'type'  => 'date_range',

            // OPTIONALS
            // default values for start_date & end_date
            // 'default'            => ['2019-03-28 01:01', '2019-04-05 02:00'],
            // options sent to daterangepicker.js
            'date_range_options' => [
                'drops' => 'auto', // can be one of [down/up/auto]
                'timePicker' => false,
                // 'locale' => ['format' => 'DD/MM/YYYY HH:mm']
            ]
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Product",
            'type'      => 'select2_multiple',
            'name'      => 'product', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'product', // the method that defines the relationship in your Model
            'model'     => "App\Models\Product", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            // 'options'   => (function ($query) {
            //     return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            // }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Store",
            'type'      => 'select2_multiple',
            'name'      => 'store', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'store', // the method that defines the relationship in your Model
            'model'     => "App\Models\StoreBranch", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            // 'options'   => (function ($query) {
            //     return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            // }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);
    }
}
