<?php

namespace App\Http\Controllers\Admin;

use App\Flag;
use App\Http\Requests\TicketRequest;
use App\Models\ProductType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TicketCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ticket');
        CRUD::setEntityNameStrings('ticket', 'tickets');
        $this->crud->setShowView('config.ticket.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addClause('whereHas', 'type', function($query) {
            $query->whereIn('flag', [Flag::ALC_RACE, Flag::ARCADE]);
        });
        if (backpack_user()->hasAnyRole(['store manager', 'regional manager'])) {
            $this->crud->removeButton('delete');
            $this->crud->addClause('whereIn', 'id', DB::table('store_branch_products')->whereIn('store_branch_id', backpack_user()->storeBranch->pluck('id'))->pluck('product_id'));
        }
        if (backpack_user()->hasRole('store manager')) {
            $this->crud->removeButton('create');
        }

        $this->crud->addFilter([
            'type'  => 'text',
            'name'  => 'name',
            'label' => 'Ticket Name'
        ],
        false,
        function($value) { // if the filter is active
            $this->crud->addClause('where', 'name', 'LIKE', "%$value%");
        });

          // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'ticket_type_id',
            'type'  => 'select2_multiple',
            'label' => 'Ticket Type'
        ], function() {
            $data = ProductType::whereIn('flag', [Flag::ALC_RACE, Flag::ARCADE])->pluck('name', 'id')->toArray();
            return $data;
        }, function($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'product_type_id', json_decode($values));
        });
        // simple filter
        $this->crud->addFilter([
            'type'  => 'simple',
            'name'  => 'active',
            'label' => 'Active'
        ],
        false,
        function() { // if the filter is active
            $this->crud->addClause('where', 'active', '=', true); // apply the "active" eloquent scope
        } );

        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Nomor',
            'orderable' => false,
        ])->makeFirstColumn();
        $this->crud->addColumns(['code', 'name']);

        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Ticket Type', // Table column heading
            'type'      => 'select',
            'name'      => 'ticket_type_id', // the column that contains the ID of that connected entity;
            'entity'    => 'type', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\TicketType", // foreign key model
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('producttype/'.$related_key.'/show');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);

        $this->crud->addColumn([
            'name'     => 'price',
            'label'    => 'Price',
            'type'     => 'closure',
            'function' => function($entry) {
                return 'Rp'.number_format($entry->price, 2, ',', '.');
            }
        ]);

        $this->crud->addColumn([
            'name'     => 'holiday_price',
            'label'    => 'Holiday Price',
            'type'     => 'closure',
            'function' => function($entry) {
                if (!empty($entry->holiday_price)) {
                    return 'Rp'.number_format($entry->holiday_price, 2, ',', '.');
                } else {
                    return 'Rp'.number_format($entry->price, 2, ',', '.');
                }
            }
        ]);

        $this->crud->addColumn([
            'name'     => 'promo_price',
            'label'    => 'Promo Price',
            'type'     => 'closure',
            'function' => function($entry) {
                if (!empty($entry->promo_price)) {
                    return 'Rp'.number_format($entry->promo_price, 2, ',', '.');
                } else {
                    return "-";
                }
            }
        ]);

        $this->crud->addColumn([
            'name'     => 'timer',
            'label'    => 'Timer',
            'type'     => 'closure',
            'function' => function($entry) {
                if (!empty($entry->timer)) {
                    return $entry->timer.' Menit';
                } else {
                    return '&infin;';
                }
            }
        ]);

        $this->crud->addColumn([
            'name'  => 'day_active', // The db column name
            'label' => 'Active Day', // Table column heading
            'type'  => 'array',
        ]);

        $this->crud->addColumn([
            'name'  => 'active',
            'label' => 'Status',
            'type'  => 'boolean',
            // optionally override the Yes/No texts
            'options' => [0 => 'Inactive', 1 => 'Active'],
            'wrapper'   => [
                'class' => function($crud, $column, $entry, $related_model_key) {
                    if($related_model_key == 1) {
                        return 'badge badge-success';
                    }
                    if($related_model_key == 0) {
                        return 'badge badge-danger';
                    }
                }
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TicketRequest::class);

        $this->crud->addFields(['code', 'name']);
        $this->crud->addField([
            'label'     => "Price",
            'type'      => 'number',
            'name'      => 'price',

            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            // 'suffix'     => ".00",
        ]);

        $this->crud->addField([
            'label'     => "Holiday Price",
            'type'      => 'number',
            'name'      => 'holiday_price',
            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            // 'suffix'     => ".00",
            'hint'      => 'ignore it if same price',
        ]);

        $this->crud->addField([
            'label'     => "Promo Price",
            'type'      => 'number',
            'name'      => 'promo_price',
            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            // 'suffix'     => ".00",
        ]);

        $this->crud->addField(
            [   // date_range
                'name'  => ['start_date_promo', 'end_date_promo'], // db columns for start_date & end_date
                'label' => 'Promo Date Range',
                'type'  => 'date_range',

                // OPTIONALS
                // default values for start_date & end_date
                // options sent to daterangepicker.js
                'date_range_options' => [
                    'drops' => 'auto', // can be one of [down/up/auto]
                    'timePicker' => false,
                    'locale' => ['format' => 'DD/MM/YYYY']
                ]
            ]
        );

        $this->crud->addField([  // Select2
            'label'     => "Ticket Type",
            'type'      => 'select2',
            'name'      => 'product_type_id', // the db column for the foreign key

            // optional
            'entity'    => 'type', // the method that defines the relationship in your Model
            'model'     => "App\Models\ProductType", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'options'   => (function ($query) {
                return $query->where('flag', '=', 1)->get();
            })
        ]);

        $this->crud->addField([   // select2_from_array
            'name'        => 'day_active',
            'label'       => "Day Active",
            'type'        => 'select2_from_array',
            'options'     => ['Sunday' => 'Minggu',
                            'Monday' => 'Senin',
                            'Tuesday' => 'Selasa',
                            'Wednesday' => 'Rabu',
                            'Thursday' => 'Kamis',
                            'Friday' => 'Jumat',
                            'Saturday' => 'Sabtu'],
            'allows_null' => false,
            'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        $this->crud->addField([   // number
            'name'        => 'timer',
            'label'       => "Timer",
            'type'        => 'number',
            'hint'        => 'Menit'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Store",
            'type'      => 'select2_multiple',
            'name'      => 'storeBranch', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'storeBranch', // the method that defines the relationship in your Model
            'model'     => "App\Models\StoreBranch", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?

            'options'   => (function ($query) {
                if (backpack_user()->hasAnyRole(['store manager', 'regional manager'])) {
                    return $query->whereIn('id', backpack_user()->storeBranch->pluck('id'))->get();
                } else {
                    return $query->get();
                }
            })
        ]);

        $this->crud->addField('active');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->addField([
            'name' => 'code',
            'type' => 'text',
            'attributes' => [
                'readonly'    => 'readonly',
              ],
        ]);
        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'attributes' => [
                'readonly'    => 'readonly',
              ],
        ]);
        $this->crud->addField([
            'label'     => "Price",
            'type'      => 'number',
            'name'      => 'price',

            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            // 'suffix'     => ".00",
        ]);

        $this->crud->addField([
            'label'     => "Holiday Price",
            'type'      => 'number',
            'name'      => 'holiday_price',
            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            // 'suffix'     => ".00",
            'hint'      => 'ignore it if same price',
        ]);

        $this->crud->addField([
            'label'     => "Promo Price",
            'type'      => 'number',
            'name'      => 'promo_price',
            // optionals
            'attributes' => ["step" => "any"], // allow decimals
            'prefix'     => "Rp",
            // 'suffix'     => ".00",
        ]);

        $this->crud->addField(
            [   // date_range
                'name'  => ['start_date_promo', 'end_date_promo'], // db columns for start_date & end_date
                'label' => 'Promo Date Range',
                'type'  => 'date_range',

                // OPTIONALS
                // default values for start_date & end_date
                // options sent to daterangepicker.js
                'date_range_options' => [
                    'drops' => 'auto', // can be one of [down/up/auto]
                    'timePicker' => false,
                    'locale' => ['format' => 'DD/MM/YYYY']
                ]
            ]
        );

        $this->crud->addField([  // Select2
            'label'     => "Ticket Type",
            'type'      => 'select2',
            'name'      => 'product_type_id', // the db column for the foreign key

            // optional
            'entity'    => 'type', // the method that defines the relationship in your Model
            'model'     => "App\Models\ProductType", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user

            'options'   => (function ($query) {
                return $query->whereNotIn('flag', [Flag::NORMAL])->get();
            })
        ]);

        $this->crud->addField([   // select2_from_array
            'name'        => 'day_active',
            'label'       => "Day Active",
            'type'        => 'select2_from_array',
            'options'     => ['Sunday' => 'Minggu',
                            'Monday' => 'Senin',
                            'Tuesday' => 'Selasa',
                            'Wednesday' => 'Rabu',
                            'Thursday' => 'Kamis',
                            'Friday' => 'Jumat',
                            'Saturday' => 'Sabtu'],
            'allows_null' => false,
            'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);

        $this->crud->addField([   // number
            'name'        => 'timer',
            'label'       => "Timer",
            'type'        => 'number',
            'hint'        => 'Menit'
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Store",
            'type'      => 'select2_multiple',
            'name'      => 'storeBranch', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'storeBranch', // the method that defines the relationship in your Model
            'model'     => "App\Models\StoreBranch", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?

            'options'   => (function ($query) {
                if (backpack_user()->hasAnyRole(['store manager', 'regional manager'])) {
                    return $query->whereIn('id', backpack_user()->storeBranch->pluck('id'))->get();
                } else {
                    return $query->get();
                }
            })
        ]);

        $this->crud->addField('active');
    }
}
