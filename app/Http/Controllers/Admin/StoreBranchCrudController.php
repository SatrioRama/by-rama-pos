<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreBranchRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StoreBranchCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StoreBranchCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StoreBranch::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/storebranch');
        CRUD::setEntityNameStrings('Store Branch', 'Store Branches');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Nomor',
            'orderable' => false,
        ])->makeFirstColumn();
        $this->crud->addColumns(['name', 'address', 'city', 'telephone', 'tax']);
        $this->crud->addColumn([
            // 1-n relationship
            'label'     => 'Store Type', // Table column heading
            'type'      => 'select',
            'name'      => 'store_branch_type_id', // the column that contains the ID of that connected entity;
            'entity'    => 'type', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\StoreBranchType", // foreign key model
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('storebranchtype/'.$related_key.'/show');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StoreBranchRequest::class);

        $this->crud->addFields(['name', 'address', 'city', 'telephone', 'type']);

        $this->crud->addField([   // color_picker
            'label'      => 'Pajak',
            'name'       => 'tax',
            'type'       => 'number',
            'suffix'     => "%",
        ]);

        $this->crud->addField([   // color_picker
            'label'      => 'Biaya Operasional',
            'name'       => 'operational',
            'type'       => 'number',
            'prefix'     => "Rp",
            'suffix'     => ".00",
        ]);

        $this->crud->addField([   // color_picker
            'label'                => 'Background Color',
            'name'                 => 'background_color',
            'type'                 => 'color_picker',
            'default'              => '#000000',

            // optional
            // Anything your define inside `color_picker_options` will be passed as JS
            // to the JavaScript plugin. For more information about the options available
            // please see the plugin docs at:
            //  ### https://itsjavi.com/bootstrap-colorpicker/module-options.html
            'color_picker_options' => [
                'customClass' => 'custom-class',
                'horizontal' => true,
                'extensions' => [
                    [
                        'name' => 'swatches', // extension name to load
                    ]
                ]
            ]
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Payment Method",
            'type'      => 'select2_multiple',
            'name'      => 'paymentMethod', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'paymentMethod', // the method that defines the relationship in your Model
            'model'     => "App\Models\PaymentMethod", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?
       ]);

        $this->crud->addField([   // Upload
            'name'      => 'logo',
            'label'     => 'Logo',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'public', // if you store files in the /public folder, please omit this; if you store them in /storage or S3, please specify it;
            // optional:
            // 'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URLs this will make a URL that is valid for the number of minutes specified
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Product",
            'type'      => 'select2_multiple',
            'name'      => 'product', // the method that defines the relationship in your Model

            // optional
            'entity'    => 'product', // the method that defines the relationship in your Model
            'model'     => "App\Models\Product", // foreign key model
            'attribute' => 'name_detail', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?

            'options'   => (function ($query) {
                $url = url()->current();
                $prefix = url('admin/storebranch/');
                $replace1 = str_replace($prefix."/","",$url);
                $replace2 = str_replace("/edit","",$replace1);
                $array = $this->crud->query->where('id', '=', $replace2)->first()->type->productType->pluck('id');
                return $query->whereIn('product_type_id', $array)->get();
            })
        ]);
    }
}
