<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VehicleRequest;
use App\Models\Vehicle;
use App\Models\VehicleTime;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class VehicleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VehicleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Vehicle::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/vehicle');
        CRUD::setEntityNameStrings('vehicle', 'vehicles');
        $this->crud->setListView('config.vehicle.listcustom');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VehicleRequest::class);

        $this->crud->addFields(['ticket', 'name', 'description', 'active']);
        $this->crud->addField([
            'name' => 'been_used',
            'type' => 'hidden'
        ]);
        $this->crud->removeSaveActions(['save_and_edit', 'save_and_preview', 'save_and_new']);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(Request $request)
    {
        $save = new Vehicle();
        $save->product_id = $request->product_id;
        $save->name = $request->name;
        $save->been_used = '00:00:00';
        $save->active = $request->active;
        $save->description = $request->description;
        $save->save();

        \Alert::add('success', 'Success added data')->flash();
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $update = Vehicle::find($id);
        $update->name = $request->name;
        $update->description = $request->description;
        $update->active = $request->active;
        $update->update();
        \Alert::add('success', 'Success change data')->flash();
        return redirect()->back();
    }

    public function getEdit(Request $request)
    {
        $vehicle = Vehicle::find($request->vehicle_id);
        if ($vehicle == null) {
            $return = array (
                'code' => 404,
                'error' => true,
                'message' => 'Data Tidak Ditemukan',
            );
        }else{
            $return = array (
                'code' => 200,
                'success' => true,
                'data' => $vehicle,
                'message' => 'Data Ditemukan',
            );
        }
        return $return;
    }

    public function destroy($id)
    {
        $this->crud->delete($id);
        \Alert::add('success', 'Success delete data')->flash();
        return redirect()->back();
    }
}
