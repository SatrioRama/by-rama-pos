<?php

namespace App\Http\Livewire;

use App\Models\VehicleTime;
use Carbon\Carbon;
use Livewire\Component;

class StatusVehicle extends Component
{
    public $vehicle;

    protected $listeners = ['time_updated' => 'render'];

    public function render()
    {
        $vehicle = $this->vehicle;
        $times = VehicleTime::where('vehicle_id', '=', $vehicle->id)->where('start_at', '<', date(Carbon::now()))->where('end_at', '>', date(Carbon::now()))->first();
        if (!empty($times)) {
            $t1 = Carbon::parse($times->end_at);
            $t2 = Carbon::now();
            $diff = $t2->diff($t1);
            $diff_times = $diff->format('%i:%s');
        }else{
            $diff_times = "00:00";
        }

        return view('livewire.status-vehicle', compact('diff_times'));
    }
}
