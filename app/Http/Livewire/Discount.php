<?php

namespace App\Http\Livewire;

use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class Discount extends Component
{
    protected $listeners = ['cart_updated' => 'render'];

    public function render()
    {
        $discount = Cart::content()->sum('weight');
        return view('livewire.discount', compact('discount'));
    }
}
