<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ReportTransaction extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'transactions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function storebranch()
    {
        return $this->belongsTo(StoreBranch::class, 'store_branch_id', 'id');
    }

    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }

    public function details()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id', 'id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method', 'id');
    }

    public function product()
    {
        return $this->hasManyThrough(Product::class, TransactionDetail::class, 'transaction_id', 'id', 'id', 'product_id');
    }

    public function productType()
    {
        return $this->hasManyDeep(
            ProductType::class,
            [TransactionDetail::class, Product::class], // Intermediate models, beginning at the far parent (Country).
            [
               'transaction_id', // Foreign key on the "transaction_details" table.
               'id',    // Foreign key on the "products" table.
               'id'     // Foreign key on the "product_types" table.
            ],
            [
              'id', // Local key on the "transactions" table.
              'product_id', // Local key on the "transaction_details" table.
              'id'  // Local key on the "products" table.
            ]
        );
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
