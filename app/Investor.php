<?php
namespace App;

class Investor {

    static function calc($oz, $op, $tax, $bh)
    {
        $tx = $oz * ($tax/100);
        $bagi = $bh/100;
        $v = ($oz - $op - $tx) * $bagi;
        return $v;
    }

}
