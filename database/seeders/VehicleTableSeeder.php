<?php

namespace Database\Seeders;

use App\Models\Vehicle;
use Illuminate\Database\Seeder;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sc01 = Vehicle::create([
            'product_id' => '4',
            'name' => 'SC-001',
            'been_used' => '10:00:00',
            'active' => '1',
        ]);
        $scoopyred = Vehicle::create([
            'product_id' => '4',
            'name' => 'SC-002 Scoopy Red',
            'been_used' => '15:00:00',
            'active' => '0',
            'description' => 'Maintenance roda',
        ]);
    }
}
