<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Satrio Rama',
            'username' => 'satrio',
            'email' => 'ramadhanu@anomali.co.id',
            'password' => bcrypt('Movy1233')
        ]);

        $user1->assignRole('superadmin');

        $admin = User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $admin->assignRole('admin');

        $direktur = User::create([
            'name' => 'direktur',
            'username' => 'direktur',
            'email' => 'direktur@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $direktur->assignRole('direktur');

        $investor = User::create([
            'name' => 'investor',
            'username' => 'investor',
            'email' => 'investor@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $investor->assignRole('investor');

        $regional_manager = User::create([
            'name' => 'regional_manager',
            'username' => 'regional_manager',
            'email' => 'regional_manager@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $regional_manager->assignRole('regional manager');

        $store_manager = User::create([
            'name' => 'store_manager',
            'username' => 'store_manager',
            'email' => 'store_manager@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $store_manager->assignRole('store manager');

        $kasir = User::create([
            'name' => 'kasir',
            'username' => 'kasir',
            'email' => 'kasir@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $kasir->assignRole('kasir');

        $operator = User::create([
            'name' => 'operator',
            'username' => 'operator',
            'email' => 'operator@byrama.com',
            'password' => bcrypt('P@ssw0rd123!')
        ]);

        $operator->assignRole('operator');

    }
}
