<?php

namespace Database\Seeders;

use App\Color;
use App\Models\StoreBranch;
use App\Models\StoreBranchType;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StartUpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i=0; $i <= 3 ; $i++) {
            $branch = StoreBranch::create([
                'name'      => $faker->company,
                'address'   => $faker->address,
                'city'      => $faker->cityName,
                'telephone' => $faker->phoneNumber,
                'tax'       => null,
                'operational'=> null,
                'edcs'       => null,
                'store_branch_type_id'=> StoreBranchType::inRandomOrder()->first()->id,
                'background_color'=> Color::hex2rgb('#'.str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT)),
            ]);
        }
    }
}
