<div class="row">
    <div class="col-md-12">
        <div class="card border-danger col-md-12">
            <div class="card-header text-center">Data Table Transaksi Bulan {{Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('M Y')}}</div>
            <div class="card-body">
                <table id="data-table" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">Nomor</th>
                            <th class="text-center">Tanggal</th>
                            @foreach (backpack_user()->storeBranch as $store)
                                <th class="text-center">{{$store->name}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($store_manager['monthly'] as $key => $date)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$date}}</td>
                            @foreach (backpack_user()->storeBranch as $store)
                                @php
                                    $data = App\Models\Transaction::where('store_branch_id', '=', $store->id)->whereDate('created_at', date('Y-m-d', strtotime($date)))->sum('total_amount');
                                @endphp
                                <td class="text-center">Rp{{number_format($data, 2, ',', '.')}}</td>
                            @endforeach
                            @php
                                $data = App\Models\Transaction::whereDate('created_at', date('Y-m-d', strtotime($date)))->sum('total_amount');
                            @endphp
                            {{-- <td class="text-center"><strong>Rp{{number_format($data, 2, ',', '.')}}</strong></td> --}}
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center" colspan="2">Total</th>
                            @foreach (backpack_user()->storeBranch as $store)
                                @php
                                    $data = App\Models\Transaction::where('store_branch_id', '=', $store->id)->whereMonth('created_at', date('m', strtotime($store_manager['monthly'][0])))->whereYear('created_at', $store_manager['yearly']['year'])->sum('total_amount');
                                @endphp
                                    <th class="text-center">Rp{{number_format($data, 2, ',', '.')}}</th>
                            @endforeach
                            @php
                                $data = App\Models\Transaction::whereMonth('created_at', date('m', strtotime($store_manager['monthly'][0])))->whereYear('created_at', $store_manager['yearly']['year'])->sum('total_amount');
                            @endphp
                                {{-- <th class="text-center">Rp{{number_format($data, 2, ',', '.')}}</th> --}}
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
