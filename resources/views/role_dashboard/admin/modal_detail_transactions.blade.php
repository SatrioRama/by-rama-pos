<!-- Modal -->
<div class="modal fade" id="modalTransactionDetails" tabindex="-1" role="dialog" aria-labelledby="modalTransactionDetailsTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document" id="fade-modal">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tanggal">Cabang <strong id="store-branch-label"></strong><br>Tanggal <strong id="date-label"></strong></h5>
          <button type="button" class="close" data-dismiss="modal" id="modal_detail_close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <table id="tabel-detail" class="table table-striped" style="width:100%">
                <thead>
                    <tr class="bg-info">
                        <th class="text-center">Nama Produk</th>
                        <th class="text-center">Total Dibeli</th>
                        <th class="text-center">Pendapatan</th>
                    </tr>
                </thead>
                <tbody id="body-table" class="text-center">
                </tbody>
                <tfoot>
                    <tr class="bg-success">
                        <th class="text-center">Total</th>
                        <th class="text-center" id="total-sold-sum"></th>
                        <th class="text-center" id="income-sum"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" id="modal_detail_close_footer" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
