<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item">
              <a class="nav-link border-info border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="yearly">Yearly</a>
            </li>
            <li class="nav-item">
              <a class="nav-link border-warning border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="monthly">Monthly</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active border-danger border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="data-stock">Data Table</a>
            </li>
            <li class="nav-item">
              <a class="nav-link border-success border-bottom-0" aria-current="page" href="{{backpack_url('dashboard')}}" id="popular">Popular</a>
            </li>
        </ul>
        <div class="card border-danger col-md-12">
            @include('role_dashboard.direktur.data-table')
        </div>
    </div>
</div>
