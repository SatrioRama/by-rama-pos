<div class="row">
    <div class="col-md-12">
        @php
        Widget::add([
            'type'    => 'div',
            'class'   => 'row',
            'content' => [ // widgets
                ['type'       => 'chart',
                'controller' => \App\Http\Controllers\Admin\Charts\RegionalManagerMonthlyChartController::class,
                'wrapper' => ['class' => 'col text-center'],
                'content' => ['header' => 'Transaksi Bulan '.Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('M Y')],
                ]
            ]
        ])->section('before_content');
        @endphp
    </div>
</div>
