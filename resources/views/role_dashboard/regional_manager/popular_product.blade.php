<div class="row">
    <div class="col-md-12">
        @php
        Widget::add([
            'type'    => 'div',
            'class'   => 'row',
            'content' => [ // widgets
                ['type'       => 'chart',
                'controller' => \App\Http\Controllers\Admin\Charts\RegionalManagerPopularProductChartController::class,
                'wrapper' => ['class' => 'col text-center'],
                'content' => ['header' => 'Product Popular Bulan '.Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('M Y')],
                ]
            ]
        ])->section('after_content');
        @endphp
    </div>
</div>
