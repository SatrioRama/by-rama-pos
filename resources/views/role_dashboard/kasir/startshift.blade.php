<!-- Modal -->
<div class="modal fade" id="startShift" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="startShiftLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="startShiftLabel">Start Cashier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('shift.store') }}" method="post" name="form_add_vehicle" id="form_add_vehicle">
                    @csrf
                    <input type="hidden" name="http_referrer" value="{{backpack_url('cashier/create')}}">
                    <input type="hidden" name="user_id" value="{{ backpack_user()->id }}">
                    <div class="input-group mb-3">
                        <span class="input-group-text" id="begining_cash">Beginning Balance</span>
                        <input type="number" name="begining_cash" class="form-control" aria-label="Sizing example input" aria-describedby="begining_cash">
                    </div>
                    <input type="hidden" name="save_action" value="save_and_back">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary" id="add-buton-out">SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
