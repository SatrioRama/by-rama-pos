@extends(backpack_view('blank'))

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card col-md-12">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <img src="/logo.png" height="100%" class="card-img-top" alt="PT. Lintang Aksa Calandra">
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h1>@if ($crud->entry->active == true)
                                Shift Sedang Berjalan
                            @else
                                Shift Selesai
                            @endif</h1>
                        </div>
                        <div class="col-md-6">
                            <h4 class="float-right" style="font-weight:100">{{ date('d M Y',strtotime($crud->entry->created_at)) }}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="card-text">Shift User : <strong>{{$crud->entry->user->name}}</strong></h4>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="table">
                        <table class="table no-border">
                            <tr>
                                <td><h4>Mulai Shift</h4></td>
                                <td><h4><strong>{{date('d M Y, H:i:s',strtotime(@$crud->entry->open_at))}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Selesai Shift</h4></td>
                                <td><h4><strong>{{date('d M Y, H:i:s',strtotime(@$crud->entry->close_at))}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Uang Tunai Saat Mulai</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->begining_cash, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Total Pemasukan Cash</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->transaction_cash, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Total Pemasukan EDC</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->transaction_edc, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Total Refund</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->refund, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Total Pemasukan</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->total_transaction, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Total Setoran Uang</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->expected_cash, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Jumlah Uang Kasir</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->actual_cash, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Selisih</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->difference, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                        </table>
                    </div>
                    <hr>
                    @php
                        $details = App\Models\TransactionDetail::whereIn('transaction_id', $crud->entry->transaction->pluck('id')->toArray())->get();
                        $item_ids = App\Models\TransactionDetail::select('product_id')->whereIn('transaction_id', $crud->entry->transaction->pluck('id')->toArray())->distinct('product_id')->pluck('product_id')->toArray();
                        $products = App\Models\Product::whereIn('id' ,$item_ids)->get();
                    @endphp
                    <div class="table">
                        <table class="table no-border">
                            <tr>
                                <td style="width: 25%; text-align: center"><h4>Item Code</h4></td>
                                <td style="width: 25%; text-align: center"><h4>Item</h4></td>
                                <td style="width: 10%; text-align: center"><h4>Quantity</h4></td>
                                <td style="width: 40%; text-align: center"><h4>Sub Total</h4></td>
                            </tr>
                            @foreach ($products as $product)
                                <tr>
                                    <td style="text-align: center">{{$product->code}}</td>
                                    <td style="text-align: center">{{$product->name}}</td>
                                    <td style="text-align: center">{{@$details->where('product_id', '=', $product->id)->sum('qty')}}</td>
                                    <td style="text-align: center">Rp{{number_format(@$details->where('product_id', '=', $product->id)->sum('sub_total'), 2, ',', '.')}}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td style="width: 40%; text-align: center" colspan="2"><h4><strong>Total</strong></h4></td>
                                <td style="width: 20%; text-align: center"><h4><strong>{{@$details->sum('qty')}}</strong></h4></td>
                                <td style="width: 40%; text-align: center"><h4><strong>Rp{{number_format(@$details->sum('sub_total'), 2, ',', '.')}}</strong></h4></td>
                            </tr>
                        </table>
                    </div>
                </ul>
                <div class="card-footer">
                    <div class="row p-2">
                        @if ($crud->entry->active == false)
                            <div class="col-md-12">
                                <a class="col-md-12 btn btn-secondary" href="{{ backpack_url('shift/struk/'.$crud->entry->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK LAPORAN</strong></a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
@endsection
