<div class="input-group mb-3">
    <span class="input-group-text col-md-3" id="total_amount">Total Amount</span>
    <span class="input-group-text col-md-1" id="cash">Rp</span>
    <input type="text" class="form-control" value="{{number_format($total, 2, ',', '.')}}" aria-label="Sizing example input" aria-describedby="total_amount" disabled>
</div>
