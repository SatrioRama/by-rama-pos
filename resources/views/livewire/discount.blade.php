<div class="input-group mb-3">
    <span class="input-group-text col-md-3" id="discount">Discount</span>
    <span class="input-group-text col-md-1" id="discount">Rp</span>
    <input type="text" class="form-control" value="{{number_format($discount, 2, ',', '.')}}" aria-label="Sizing example input" aria-describedby="discount" disabled>
    <input type="hidden" class="form-control" name="discount" value="{{$discount}}" aria-label="Sizing example input" aria-describedby="discount" readonly>
</div>
