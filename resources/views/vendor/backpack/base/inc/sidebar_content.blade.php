<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
@if (backpack_user()->hasRole('operator') && count(backpack_user()->storeBranch) == 1)
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('vehicle') }}'><i class='nav-icon las la-car-side'></i> Vehicles</a></li>
@elseif (backpack_user()->hasRole('kasir'))
    @if (!empty(\App\Models\Shift::where('user_id', '=', @backpack_user()->id)->where('store_branch_id', '=', @backpack_user()->storeBranch->first()->id)->where('active', '=', App\Status::ACTIVE)->first()))
        <li class='nav-item'><a class='nav-link' href='{{ route('transaction.create') }}'><i class='nav-icon las la-shopping-cart'></i> Chart</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon las la-luggage-cart'></i> List Transaction</a></li>
        @if (count(\App\Models\Vehicle::where('active', '=', 1)->whereHas('product', function ($query) {
            return $query->whereHas('storeBranch', function ($query) {
            return $query->where('store_branches.id', '=', backpack_user()->storeBranch->first()->id);
        });})->orderBy('name')->get()) > 0)
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('vehicle') }}'><i class='nav-icon las la-car-side'></i> List Wahana</a></li>
        @endif
    @else
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shift') }}'><i class='nav-icon las la-clock'></i> Report Shift</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon las la-luggage-cart'></i> Report Transaction</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
    @endif
@elseif (backpack_user()->hasRole('store manager'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon las la-shopping-cart'></i> Transaction</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shift') }}'><i class='nav-icon las la-clock'></i> Report Shift</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('report-transaction') }}'><i class='nav-icon las la-luggage-cart'></i> Report Transactions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-cogs"></i> Config</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket') }}'><i class='nav-icon las la-ticket-alt'></i> Tickets</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon las la-box'></i> Products</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        </ul>
    </li>
@elseif (backpack_user()->hasRole('regional manager'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon las la-shopping-cart'></i> Transaction</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shift') }}'><i class='nav-icon las la-clock'></i> Report Shift</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('report-transaction') }}'><i class='nav-icon las la-luggage-cart'></i> Report Transactions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-cogs"></i> Config</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket') }}'><i class='nav-icon las la-ticket-alt'></i> Tickets</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon las la-box'></i> Products</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('storebranch') }}'><i class='nav-icon las la-warehouse'></i> Store Branches</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        </ul>
    </li>
@elseif (backpack_user()->hasRole('investor'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
@elseif (backpack_user()->hasRole('direktur'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shift') }}'><i class='nav-icon las la-clock'></i> Report Shift</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('report-transaction') }}'><i class='nav-icon las la-luggage-cart'></i> Report Transactions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
@elseif (backpack_user()->hasRole('admin'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon las la-shopping-cart'></i> Transaction</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shift') }}'><i class='nav-icon las la-clock'></i> Report Shift</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('report-transaction') }}'><i class='nav-icon las la-luggage-cart'></i> Report Transactions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket') }}'><i class='nav-icon las la-ticket-alt'></i> Tickets</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon las la-box'></i> Products</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('producttype') }}'><i class='nav-icon las la-boxes'></i> Product Types</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('storebranch') }}'><i class='nav-icon las la-warehouse'></i> Store Branches</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('storebranchtype') }}'><i class='nav-icon las la-city'></i> Store Branch Types</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment-method') }}'><i class='nav-icon la la-money-bill-wave'></i> Payment Methods</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('voucher') }}'><i class='nav-icon la la-tags'></i> Vouchers</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('holiday') }}'><i class='nav-icon la la-gift'></i> Holidays</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('good-inbound') }}'><i class='nav-icon la la-box'></i> Good Inbounds</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
@elseif (backpack_user()->hasRole('superadmin'))
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction') }}'><i class='nav-icon las la-shopping-cart'></i> Transaction</a></li>
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('vehicle') }}'><i class='nav-icon las la-car-side'></i> Vehicles</a></li> --}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('shift') }}'><i class='nav-icon las la-clock'></i> Report Shift</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('report-transaction') }}'><i class='nav-icon las la-luggage-cart'></i> Report Transactions</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('transaction-detail') }}'><i class='nav-icon las la-cart-arrow-down'></i> Report Transaction Detail</a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-cogs"></i> Config</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket') }}'><i class='nav-icon las la-ticket-alt'></i> Tickets</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon las la-box'></i> Products</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('producttype') }}'><i class='nav-icon las la-boxes'></i> Product Types</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('storebranch') }}'><i class='nav-icon las la-warehouse'></i> Store Branches</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('storebranchtype') }}'><i class='nav-icon las la-city'></i> Store Branch Types</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment-method') }}'><i class='nav-icon la la-money-bill-wave'></i> Payment Methods</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('voucher') }}'><i class='nav-icon la la-tags'></i> Vouchers</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('holiday') }}'><i class='nav-icon la la-gift'></i> Holidays</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('good-inbound') }}'><i class='nav-icon la la-box'></i> Good Inbounds</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
@else
@endif
