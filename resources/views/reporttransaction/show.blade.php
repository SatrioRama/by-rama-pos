@extends(backpack_view('blank'))

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card col-md-12">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <img src="/logo.png" height="100%" class="card-img-top" alt="PT. Lintang Aksa Calandra">
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h1>Transaksi Sukses</h1>
                        </div>
                        <div class="col-md-6">
                            <h4 class="float-right" style="font-weight:100">{{ date('d M Y, h:i',strtotime($crud->entry->created_at)) }}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="card-text">Nomor Transaksi : <strong>{{$crud->entry->transaction_number}}</strong></h4>
                </div>
                <ul class="list-group list-group-flush">
                    <div class="table">
                        <table class="table no-border">
                            <tr>
                                <td><h4>Subtotal</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->total_amount, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Pembayaran Tunai</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->cash, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Kembali</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->change, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                        </table>
                    </div>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card col-md-12">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h1>Detail Transaksi</h1>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="transaction_detail" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nomor</th>
                                <th>Produk</th>
                                <th>Quantity</th>
                                <th>Harga</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($crud->entry->details as $key=>$item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->product->name}}</td>
                                    <td>{{number_format($item->qty)}}</td>
                                    <td>{{'Rp'.number_format($item->price, 2, ',', '.')}}</td>
                                    <td>{{'Rp'.number_format($item->sub_total, 2, ',', '.')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Total</th>
                                <th>{{'Rp'.number_format($crud->entry->details->sum('sub_total'), 2, ',', '.')}}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
@endsection
