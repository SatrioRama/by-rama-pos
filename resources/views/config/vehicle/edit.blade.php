<!-- Modal -->
<div class="modal fade" id="editModalVehicle" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="editModalVehicleLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalVehicleLabel">Edit Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger" id="form-modal-alert" style="display:none;">Data telah tersimpan</div>
                    <form action="#" method="post" name="form_edit_vehicle" id="form_edit_vehicle">
                        @csrf
                        @method('PUT')
                    <input type="hidden" name="http_referrer" value="{{backpack_url('ticket/'.$crud->entry->id.'/show')}}">
                    <input type="hidden" name="ticket" value="{{ $crud->entry->id }}">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="name">Name</span>
                        </div>
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control" aria-label="Default" aria-describedby="name">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Description</span>
                        </div>
                        <textarea name="description" class="form-control" aria-label="With textarea">{{ old('description') }}</textarea>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="inputGroupSelect01">Active</label>
                        </div>
                        <select name="active" class="custom-select" id="active">
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                    </div>
                    <input type="hidden" name="save_action" value="save_and_back">
                        <div class="form-group text-right">
                            <label for=""></label>
                            <button type="submit" class="btn btn-primary" id="add-buton-out">CHANGE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
