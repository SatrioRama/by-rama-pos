<div class="row">
    <div class="col-md-12">
        <div class="card col-md-12">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <img src="/logo.png" height="100%" class="card-img-top" alt="PT. Lintang Aksa Calandra">
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Transaksi Sukses</h1>
                    </div>
                    <div class="col-md-6">
                        <h4 class="float-right" style="font-weight:100">{{ date('d M Y, h:i',strtotime($data->created_at)) }}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h4 class="card-text">Nomor Transaksi : <strong>{{$data->transaction_number}}</strong></h4>
            </div>
            <ul class="list-group list-group-flush">
                <div class="table">
                    <table class="table no-border">
                        <tr>
                            <td><h4>Subtotal</h4></td>
                            <td><h4><strong>Rp{{number_format(@$data->total_amount, 2, ',', '.')}}</strong></h4></td>
                        </tr>
                        <tr>
                            <td><h4>Pembayaran Tunai</h4></td>
                            <td><h4><strong>Rp{{number_format(@$data->cash, 2, ',', '.')}}</strong></h4></td>
                        </tr>
                        <tr>
                            <td><h4>Kembali</h4></td>
                            <td><h4><strong>Rp{{number_format(@$data->change, 2, ',', '.')}}</strong></h4></td>
                        </tr>
                        @if (!empty(@$data->refund))
                        <tr>
                            <td><h4>Refund</h4></td>
                            <td><h4><strong>Rp{{number_format(@$data->refund, 2, ',', '.')}}</strong></h4></td>
                        </tr>
                        @endif
                    </table>
                </div>
            </ul>
            <div class="card-footer">
                <div class="row p-2">
                    @if (!backpack_user()->hasAnyRole(['operator', 'kasir']))
                        @if ($data->productType->where('flag', '=', \App\Flag::ALC_RACE)->count('id') > 0)
                            <div class="col-md-6">
                                <a onclick="printStruk(this.getAttribute('route-id'))" class="col-md-12 btn btn-secondary" route-id="{{ backpack_url('transaction/struk/'.$data->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK ULANG STRUK BELANJA</strong></a>
                            </div>
                            <div class="col-md-6">
                                <a onclick="printTicket(this.getAttribute('route-id'))" class="col-md-12 btn btn-primary" route-id="{{ backpack_url('transaction/ticket/'.$data->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK TIKET WAHANA</strong></a>
                            </div>
                        @else
                            <div class="col-md-12">
                                <a onclick="printStruk(this.getAttribute('route-id'))" class="col-md-12 btn btn-secondary" route-id="{{ backpack_url('transaction/struk/'.$data->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK ULANG STRUK BELANJA</strong></a>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
