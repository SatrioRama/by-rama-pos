<div class="row">
    <div class="col-md-12">
        <div class="card col-md-12">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h1>Detail Transaksi</h1>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="transaction_detail" class="table table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Produk</th>
                            <th>Quantity</th>
                            <th>Harga</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data->details as $key=>$item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->product->name}}</td>
                                <td>{{number_format($item->qty)}}</td>
                                <td>{{'Rp'.number_format($item->price, 2, ',', '.')}}</td>
                                <td>{{'Rp'.number_format($item->sub_total, 2, ',', '.')}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Total</th>
                            <th>{{'Rp'.number_format($data->details->sum('sub_total'), 2, ',', '.')}}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
