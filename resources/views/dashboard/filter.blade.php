<div class="row">
    <div class="col-md-12 text-right">
        <form class="form-inline" action="/admin/dashboard">
          <input type="month" class="form-control mb-4 mr-sm-2" name="show_dashboard_date" id="show-dashboard-date" value="{{ old('show_dashboard_date', request('show_dashboard_date')) }}">
          @if (backpack_user()->hasAnyRole(['superadmin', 'admin', 'direktur']))
            <select class="form-control mb-4 mr-sm-2" name="choose_branch" id="choose-branch">
                <option value="all">Semua Branch</option>
                @if (backpack_user()->hasAnyRole(['store manager', 'regional manager', 'investor']))
                    @foreach (\App\Models\StoreBranch::whereIn('store_branch_id', backpack_user()->storeBranch->pluck('id'))->get() as $store)
                        <option value="{{$store->id}}">{{$store->name}}</option>
                    @endforeach
                @else
                    @foreach (\App\Models\StoreBranch::get() as $store)
                        <option value="{{$store->id}}" @if (old('choose_branch', request('choose_branch')) == $store->id)
                            selected
                        @endif>{{$store->name}}</option>
                    @endforeach
                @endif
            </select>
          @endif
          <button type="submit" style="background:#4880D0;color:white;" class="form-control badge-primary mb-4 mr-sm-2"><i class="fa fa-retweet"></i> Check</button>
        </form>
    </div>
</div>
